package free.ssharyk.freestars

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import free.ssharyk.gamemanager.gameplay.SkyGenerator
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        debug()

        generateSky()
    }

    private fun generateSky() {
        frameSky.sky = SkyGenerator.random(12)
    }

    private fun debug() {
        if (BuildConfig.DEBUG) {
            cmdDebug.visibility = View.VISIBLE
            cmdDebug.setOnClickListener {
                generateSky()
            }
        } else {
            cmdDebug.visibility = View.GONE
            cmdDebug.setOnClickListener(null)
        }
    }
}
