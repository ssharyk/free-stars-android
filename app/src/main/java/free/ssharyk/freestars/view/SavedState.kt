package free.ssharyk.freestars.view

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View

internal class SavedState : View.BaseSavedState {

    var childrenStates: Bundle? = null

    constructor(superState: Parcelable?) : super(superState)

    constructor(source: Parcel) : super(source) {
        childrenStates = source.readBundle(javaClass.classLoader)
    }

    override fun writeToParcel(out: Parcel, flags: Int) {
        super.writeToParcel(out, flags)
        out.writeBundle(childrenStates)
    }

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<SavedState> {
            override fun createFromParcel(source: Parcel) = SavedState(source)
            override fun newArray(size: Int): Array<SavedState?> = arrayOfNulls(size)
        }
    }
}