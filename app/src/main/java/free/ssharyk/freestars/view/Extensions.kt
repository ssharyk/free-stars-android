package free.ssharyk.freestars.view

import android.graphics.Canvas
import android.graphics.Paint

fun Canvas.drawLine(x1: Int, y1: Int, x2: Int, y2: Int, paint: Paint) =
    this.drawLine(x1.toFloat(), y1.toFloat(), x2.toFloat(), y2.toFloat(), paint)