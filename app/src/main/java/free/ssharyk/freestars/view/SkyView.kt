package free.ssharyk.freestars.view

import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import free.ssharyk.freestars.R
import free.ssharyk.gamemanager.gameplay.SkyValidator
import free.ssharyk.gamemanager.model.Edge
import free.ssharyk.gamemanager.model.Sky
import free.ssharyk.gamemanager.model.Star
import kotlin.math.floor
import kotlin.properties.Delegates

class SkyView : View {

    companion object {
        private const val LEVEL_SIZE = 25
        private const val EDGE_STROKE_WIDTH = 20.0f

        private const val STATE_KEY_SKY = "free.ssharyk.freestars.view.STATE_KEY_SKY"
    }

    // region Properties

    var sky: Sky by Delegates.observable(Sky(emptyList(), emptyList())) { _, oldV, newV ->
        if (oldV == newV) return@observable
        invalidate()
    }

    private val drawableStar = ContextCompat.getDrawable(context, R.drawable.star)

    private val paintEdgeValid by lazy {
        Paint().apply {
            val colors = intArrayOf(Color.MAGENTA, Color.CYAN, Color.MAGENTA)
            this.shader = LinearGradient(
                0f, 0f, width.toFloat(), height.toFloat(),
                colors, null, Shader.TileMode.REPEAT
            )
            this.strokeWidth = EDGE_STROKE_WIDTH
        }
    }

    private val paintEdgeIntersection by lazy {
        Paint().apply {
            val colors = intArrayOf(Color.YELLOW, Color.RED, Color.YELLOW)
            this.shader = LinearGradient(
                0f, 0f, width.toFloat(), height.toFloat(),
                colors, null, Shader.TileMode.REPEAT
            )
            this.strokeWidth = EDGE_STROKE_WIDTH
        }
    }

    // endregion

    // region Construction

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int)
            : super(context, attrs, defStyle) {
        init(context)
    }

    private fun init(context: Context) {
    }

    // endregion

    // region Draw

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (canvas == null) return

        this.drawEdges(canvas)
        this.drawStars(canvas)
    }

    private fun drawStars(canvas: Canvas) {
        for (star in sky.stars) {
            drawStar(canvas, star)
        }
    }

    private fun drawStar(canvas: Canvas, star: Star) {
        drawableStar?.let {
            it.setBounds(star.left, star.top, star.right, star.bottom)
            it.draw(canvas)
        }
    }

    private fun drawEdges(canvas: Canvas) {
        for (edge in sky.edges) {
            drawEdge(canvas, edge)
        }
    }

    private fun drawEdge(canvas: Canvas, edge: Edge) {
        val paint = if (edge.hasIntersections) paintEdgeIntersection else paintEdgeValid
        val centerFrom = edge.starFrom.center
        val centerTo = edge.starTo.center
        canvas.drawLine(centerFrom.x, centerFrom.y, centerTo.x, centerTo.y, paint)
    }

    // endregion

    // region Models Extensions

    private val Star.center
        get() = Point(this.x * width / 100, this.y * height / 100)

    private val Star.left
        get() = center.x - this.level * LEVEL_SIZE / 2

    private val Star.top
        get() = center.y - this.level * LEVEL_SIZE / 2

    private val Star.right
        get() = center.x + this.level * LEVEL_SIZE / 2

    private val Star.bottom
        get() = center.y + this.level * LEVEL_SIZE / 2

    // endregion

    // region Lifecycle

    private fun refresh() {
        SkyValidator.validate(sky)
        invalidate()
    }

    // endregion

    // Touch

    private var capturedStarIndex: Int? = null

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event ?: return false

        if (event.pointerCount != 1) return false

        if (event.action == MotionEvent.ACTION_DOWN) {
            capturedStarIndex = null
        }

        val starIndex = capturedStarIndex ?: starUnder(event.x, event.y)
        if (starIndex == -1) {
            return false
        }
        capturedStarIndex = starIndex

        val xPercent = floor(event.x * 100 / width).toInt()
        val yPercent = floor(event.y * 100 / height).toInt()
        sky.stars[starIndex].moveTo(xPercent, yPercent)

        refresh()

        return true
    }

    /**
     * Checks if point is inside `Star` sprite
     */
    private fun starUnder(x: Float, y: Float): Int {
        return sky.stars.indexOfFirst {
            it.left <= x && x <= it.right && it.top <= y && y <= it.bottom
        }
    }

    // endregion

    // region Config changes

    override fun onSaveInstanceState(): Parcelable? {
        return SavedState(super.onSaveInstanceState()).apply {
            childrenStates = Bundle().apply {
                putParcelable(STATE_KEY_SKY, sky)
            }
        }
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        when (state) {
            is SavedState -> {
                super.onRestoreInstanceState(state.superState)
                state.childrenStates?.let {
                    sky = it.getParcelable(STATE_KEY_SKY)!!
                }
            }
            else -> {
                super.onRestoreInstanceState(state)
            }
        }
    }

    // endregion

    // region Support

    private fun log(message: String) {
        println("SkyView > $message")
    }

    // endregion
}