package free.ssharyk.gamemanager.intersection

import free.ssharyk.gamemanager.model.Edge
import free.ssharyk.gamemanager.model.Star
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.util.stream.Stream

private class IntersectionCheckerTest {
    @ParameterizedTest
    @ArgumentsSource(CommonStarProvider::class)
    fun whenHasCommonStar_thenNoIntersection(arg: StarsArgument) {
        val edge1 = Edge(1L, arg.star11, arg.star12)
        val edge2 = Edge(2L, arg.star21, arg.star22)
        Assertions.assertTrue(IntersectionChecker.hasCommonStar(edge1, edge2))
        Assertions.assertFalse(IntersectionChecker.hasIntersection(edge1, edge2))
    }

    @ParameterizedTest
    @ArgumentsSource(IntersectionProvider::class)
    fun whenHasCommonPoint_thenIntersection(arg: StarsWithCommonArgument) {
        val edge1 = Edge(1L, arg.star11, arg.star12)
        val edge2 = Edge(2L, arg.star21, arg.star22)
        if (arg.hasCommon)
            Assertions.assertTrue(IntersectionChecker.hasCommonStar(edge1, edge2))
        else
            Assertions.assertFalse(IntersectionChecker.hasCommonStar(edge1, edge2))
        Assertions.assertTrue(IntersectionChecker.hasIntersection(edge1, edge2))
    }

    @ParameterizedTest
    @ArgumentsSource(NoIntersectionsProvider::class)
    fun whenNoCommonPoint_thenNoIntersection(arg: StarsArgument) {
        val edge1 = Edge(1L, arg.star11, arg.star12)
        val edge2 = Edge(2L, arg.star21, arg.star22)
        Assertions.assertFalse(IntersectionChecker.hasCommonStar(edge1, edge2))
        Assertions.assertFalse(IntersectionChecker.hasIntersection(edge1, edge2))
    }
}


private class CommonStarProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
        StarsArgument(Star(1L, 0, 0), Star(2L, 1, 1), Star(3L, 0, 0), Star(4L, -5, -1)),
        StarsArgument(Star(1L, 1, 1), Star(2L, 0, 0), Star(3L, 0, 0), Star(4L, -5, -1)),
        StarsArgument(Star(1L, 0, 0), Star(2L, 1, 1), Star(3L, -5, -1), Star(4L, 0, 0)),
        StarsArgument(Star(1L, 1, 1), Star(2L, 0, 0), Star(3L, -5, -1), Star(4L, 0, 0))
    ).map { Arguments.of(it) }
}

private class IntersectionProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
        StarsWithCommonArgument(Star(1L, -2, -2), Star(2L, 2, 2), Star(3L, -1, 1), Star(4L, 1, -1), false),
        StarsWithCommonArgument(Star(1L, 0, 0), Star(2L, 1, 1), Star(3L, -1, 1), Star(4L, 1, -1), false),
        StarsWithCommonArgument(Star(1L, 0, 0), Star(2L, 1, 1), Star(3L, 0, 0), Star(4L, 2, 2), true),
        StarsWithCommonArgument(Star(1L, 0, 0), Star(2L, 2, 2), Star(3L, 1, 1), Star(4L, -1, -1), false),
        StarsWithCommonArgument(Star(1L, 0, 0), Star(2L, 1, 1), Star(3L, 5, 5), Star(4L, -5, -5), false)
    ).map { Arguments.of(it) }
}

private class NoIntersectionsProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
        StarsArgument(Star(1L, 1, 1), Star(2L, 2, 2), Star(3L, -1, 1), Star(4L, 1, -1)),
        StarsArgument(Star(1L, 1, 1), Star(2L, 2, 2), Star(3L, -1, -1), Star(4L, 0, 0)),
        StarsArgument(Star(1L, 1, 1), Star(2L, 2, 2), Star(3L, 0, -1), Star(4L, 1, 0))
    ).map { Arguments.of(it) }
}

private data class StarsArgument(
    val star11: Star,
    val star12: Star,
    val star21: Star,
    val star22: Star
)

private data class StarsWithCommonArgument(
    val star11: Star,
    val star12: Star,
    val star21: Star,
    val star22: Star,
    val hasCommon: Boolean
)