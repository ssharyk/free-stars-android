package free.ssharyk.gamemanager.gameplay.generator

internal enum class SkyGeneratorType(val code: Int) {
    TREE(1),
    CIRCLE(2),
    RANDOM(0),
}