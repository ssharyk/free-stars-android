package free.ssharyk.gamemanager.gameplay.generator

import free.ssharyk.gamemanager.gameplay.generator.ISkyGenerator.Companion.BORDERS_PADDING
import free.ssharyk.gamemanager.model.Edge
import free.ssharyk.gamemanager.model.Sky
import free.ssharyk.gamemanager.model.Star
import kotlin.random.Random

internal class RandomSkyGenerator : ISkyGenerator {

    override fun create(starsCount: Int, edgesCount: Int): Sky {
        val stars = mutableListOf<Star>()
        for (i in 1..starsCount) {
            val x = BORDERS_PADDING + Random.nextInt(100 - 2 * BORDERS_PADDING)
            val y = BORDERS_PADDING + Random.nextInt(100 - 2 * BORDERS_PADDING)
            val star = Star(i.toLong(), x, y)
            stars.add(star)
        }

        var edgesCountActual = 0L
        val edges = mutableListOf<Edge>()
        while (edgesCountActual < edgesCount) {
            val fromIndex = Random.nextInt(starsCount)
            val toIndex = Random.nextInt(starsCount)
            if (fromIndex != toIndex) {
                val edge = Edge(edgesCountActual, stars[fromIndex], stars[toIndex])
                edges.add(edge)

                edgesCountActual++
            }
        }

        return Sky(stars, edges)
    }

}