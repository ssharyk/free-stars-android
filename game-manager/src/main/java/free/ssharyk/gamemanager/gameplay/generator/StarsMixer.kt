package free.ssharyk.gamemanager.gameplay.generator

import free.ssharyk.gamemanager.gameplay.generator.ISkyGenerator.Companion.BORDERS_PADDING
import free.ssharyk.gamemanager.model.Star
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

internal object StarsMixer {

    /**
     * Moves each star in a random way
     */
    fun mix(stars: List<Star>) {
        if (stars.isEmpty()) return

        normalize(stars)
        stars.forEach { moveStarRandomly(it) }
    }


    /**
     * Maps internal representation of [Star.x] and [Star.y] in [stars]
     * to area 100 x 100
     */
    private fun normalize(stars: List<Star>) {
        val xs = stars.map { it.x }
        val ys = stars.map { it.y }

        val allowedArea = 100 - 2 * BORDERS_PADDING
        val dx = xs.max()!! - xs.min()!!
        val dy = ys.max()!! - ys.min()!!

        stars.forEach {
            it.moveTo(allowedArea * it.x / dx, allowedArea * it.y / dy)
        }
    }

    /**
     * Makes random movement for the [star] (random angle, random length of the transition)
     */
    private fun moveStarRandomly(star: Star) {
        val angle = Random.nextInt(360)
        val r = 10 + Random.nextInt(100)

        val dx = (r * cos(angle.toDouble())).toInt()
        val dy = (r * sin(angle.toDouble())).toInt()

        val x = abs((star.x + dx) % 100)
        val y = abs((star.y + dy) % 100)
        star.moveTo(x, y)
    }
}