package free.ssharyk.gamemanager.gameplay.generator

import free.ssharyk.gamemanager.model.Sky

interface ISkyGenerator {

    companion object {
        const val BORDERS_PADDING = 5
    }

    fun create(starsCount: Int, edgesCount: Int): Sky
}