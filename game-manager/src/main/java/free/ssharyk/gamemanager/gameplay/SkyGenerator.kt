package free.ssharyk.gamemanager.gameplay

import free.ssharyk.gamemanager.gameplay.generator.*
import free.ssharyk.gamemanager.gameplay.generator.RandomSkyGenerator
import free.ssharyk.gamemanager.gameplay.generator.SkyGeneratorType
import free.ssharyk.gamemanager.gameplay.generator.TreeSkyGenerator
import free.ssharyk.gamemanager.model.Sky
import kotlin.random.Random

object SkyGenerator {

    private const val POSSIBLE_GENERATORS = 2

    fun random(starsCount: Int = 8, edgesCount: Int = starsCount + 5): Sky {
        val generator = createGenerator()
        val sky = generator.create(starsCount, edgesCount)
        SkyValidator.validate(sky)
        return sky
    }

    private fun createGenerator(): ISkyGenerator {
        val rnd = Random.nextInt(POSSIBLE_GENERATORS) + 1
        return when (rnd) {
            SkyGeneratorType.TREE.code -> TreeSkyGenerator()
            SkyGeneratorType.CIRCLE.code -> CircleSkyGenerator()
            else -> RandomSkyGenerator()
        }
    }
}