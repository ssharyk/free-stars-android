package free.ssharyk.gamemanager.gameplay.generator

import free.ssharyk.gamemanager.model.Edge
import free.ssharyk.gamemanager.model.Sky
import free.ssharyk.gamemanager.model.Star
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

class CircleSkyGenerator : ISkyGenerator {

    companion object {
        private const val WIDTH = 10000
        private const val HEIGHT = 10000
    }

    private var createdEdges = 0

    override fun create(starsCount: Int, edgesCount: Int): Sky {
        val stars = generateStars(starsCount)
        val edges = generateEdges(stars, edgesCount)

        // move to random place
        StarsMixer.mix(stars)
        return Sky(stars, edges)
    }

    private fun generateStars(starsCount: Int): List<Star> {

        val stars = ArrayList<Star>()
        val centerX = WIDTH / 2
        val centerY = HEIGHT / 2
        for (id in 1..starsCount) {

            val angle = 2 * PI * id / starsCount
            val x = (WIDTH / 2 * cos(angle) + centerX).toInt()
            val y = (HEIGHT / 2 * sin(angle) + centerY).toInt()

            stars.add(Star(id.toLong(), x, y))
        }

        return stars
    }

    private fun generateEdges(stars: List<Star>, edgesCount: Int): List<Edge> {
        val edges = ArrayList<Edge>()

        // circle itself
        for (i in 0 until stars.size - 1) {
            if (createdEdges >= edgesCount) break

            val edge = Edge((++createdEdges).toLong(), stars[i], stars[i + 1])
            edges.add(edge)
        }
        edges.add(Edge((++createdEdges).toLong(), stars.first(), stars.last()))

        // links between some random points on the circle
        val itemsRest = (edgesCount - createdEdges) * 3
        val newEdges = ArrayList<Edge>()
        for (i in 0..itemsRest) {
            if (createdEdges >= edgesCount) break

            var startPoint = Random.nextInt(stars.size)
            var finishPoint = Random.nextInt(stars.size)
            if (startPoint == finishPoint) continue
            if (startPoint > finishPoint) {
                val t = startPoint
                startPoint = finishPoint
                finishPoint = t
            }
            if (isInCircle(stars.size, startPoint.toLong() + 1, finishPoint.toLong() + 1))
                continue

            if (!hasEdgesInBetween(
                    newEdges,
                    (startPoint + 1).toLong(),
                    (finishPoint + 1).toLong()
                )
            ) {
                val edge = Edge((++createdEdges).toLong(), stars[startPoint], stars[finishPoint])
                newEdges.add(edge)
            }
        }

        return edges.apply { addAll(newEdges) }
    }

    private fun hasEdgesInBetween(
        edges: List<Edge>,
        startPointId: Long,
        finishPointId: Long
    ): Boolean {
        for (edge in edges) {
            if (startPointId > edge.starFrom.id && startPointId < edge.starTo.id) {
                return finishPointId >= edge.starFrom.id || finishPointId <= edge.starTo.id
            }

            if (finishPointId > edge.starFrom.id && finishPointId < edge.starTo.id) {
                return startPointId >= edge.starFrom.id || startPointId <= edge.starTo.id
            }
        }

        return false
    }

    private fun isInCircle(starsCount: Int, starFromId: Long, starToId: Long): Boolean {
        return starToId == starFromId + 1 ||
                (starFromId == starsCount.toLong() && starToId == 1L) ||
                (starToId == starsCount.toLong() && starFromId == 1L)
    }
}