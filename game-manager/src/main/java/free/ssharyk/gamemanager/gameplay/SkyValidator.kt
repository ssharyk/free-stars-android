package free.ssharyk.gamemanager.gameplay

import free.ssharyk.gamemanager.intersection.IntersectionChecker
import free.ssharyk.gamemanager.model.Sky

object SkyValidator {
    /**
     * Returns `true` if no intersections found and `false`otherwise
     */
    fun validate(sky: Sky): Boolean {
        sky.edges.forEach {
            it.reset()
        }

        var intersectionsFound = false
        for (i in 0 until sky.edges.size - 1) {
            for (j in i + 1 until sky.edges.size) {
                val e1 = sky.edges[i]
                val e2 = sky.edges[j]

                if (IntersectionChecker.hasIntersection(e1, e2)) {
                    intersectionsFound = true
                    e1.intersections.add(e2.id)
                    e2.intersections.add(e1.id)
                }
            }
        }

        return intersectionsFound
    }
}