package free.ssharyk.gamemanager.gameplay.generator

import free.ssharyk.gamemanager.model.Edge
import free.ssharyk.gamemanager.model.Sky
import free.ssharyk.gamemanager.model.Star
import kotlin.random.Random

internal class TreeSkyGenerator : ISkyGenerator {

    companion object {
        private const val DEFAULT_BRANCHES_COUNT = 3

        private const val WIDTH = 10000  // big enough tp avoid accuracy losing
    }

    // region Fields and properties

    private var starsCreated: Long = 0
    private var edgesCreated: Long = 0
    private var totalLevels: Int = 0

    private var branchesCount: Int = DEFAULT_BRANCHES_COUNT

    // endregion

    // region Sky global

    override fun create(starsCount: Int, edgesCount: Int): Sky {

        val stars = generateStars(starsCount)
        totalLevels = stars.map { it.y }.max()!!

        val edges = generateEdges(edgesCount, stars)

        // move to random place
        StarsMixer.mix(stars)

        return Sky(stars, edges)

    }

    // endregion

    // region Stars

    private fun generateStars(starsCount: Int): List<Star> {
        val x = WIDTH / 2
        val root = Star(++starsCreated, x, 0)

        val stars = mutableListOf(root)

        var currentLevelStars = listOf(root)
        var currentLevel = 0
        var currentWidth = WIDTH

        while (stars.size < starsCount) {

            // next tree level, if not enough
            var nextLevelStars = listOf<Star>()
            for (s in currentLevelStars) {
                nextLevelStars = createBranch(currentLevel + 1, starsCount, s.x, currentWidth)
                stars.addAll(nextLevelStars)
            }

            currentLevelStars = nextLevelStars
            currentLevel++
            currentWidth /= branchesCount
        }

        return stars
    }

    /**
     * Creates [branchesCount] stars on [currentLevel]+1 level of the tree according to root with coordinate [xRoot] and in area with [width]
     * In horizontal order. If stars count is less than [starsCount] than next tree level created recursively
     */
    private fun createBranch(
        currentLevel: Int,
        starsCount: Int,
        xRoot: Int,
        width: Int
    ): List<Star> {
        val currentLevelStars = mutableListOf<Star>()

        val dx = width / branchesCount
        val x0 = xRoot - width / 2 + dx / 2

        // first, create all required items in line
        for (i in 0 until branchesCount) {
            if (starsCreated >= starsCount) {
                return currentLevelStars
            }

            val x = x0 + dx * i
            val star = Star(++starsCreated, x, currentLevel)
            currentLevelStars.add(star)
        }

        return currentLevelStars
    }

    // endregion

    // region Edges

    private fun generateEdges(edgesCount: Int, stars: List<Star>): List<Edge> {
        val edges = mutableListOf<Edge>()

        edges.addAll(createEdgesParentChild(edgesCount, stars))
        edges.addAll(createEdgesNeighbours(edgesCount, stars))

        return edges
    }

    /**
     * Creates set of edges between (sub-)roots and their children
     */
    private fun createEdgesParentChild(edgesCount: Int, stars: List<Star>): List<Edge> {
        val edges = mutableListOf<Edge>()
        for (s in stars) {
            val startIdTo = branchesCount * s.id.toInt() - 1
            val finishIdTo = startIdTo + branchesCount
            var next = 0
            do {
                val toIndex = startIdTo + next - 1

                if (toIndex >= finishIdTo - 1) break
                if (toIndex >= stars.size) break

                val starTo = stars[toIndex]
                ++next

                val edge = Edge(++edgesCreated, s, starTo)
                edges.add(edge)
            } while (true)
        }
        return edges
    }

    /**
     * Creates set of edges between items in the same horizontal level but different subroots
     */
    private fun createEdgesNeighbours(edgesCount: Int, stars: List<Star>): List<Edge> {
        val edges = mutableListOf<Edge>()
        for (i in 1 until stars.size - 1) {
            if (stars[i].y == stars[i + 1].y) {

                // with some possibility, replace edge between horizontal items to edge between subroot and first children of neighbour's subroot
                val possibility = Random.nextDouble()
                val starTo =
                    if (stars[i].y in 1 until totalLevels && possibility > 0.7) {
                        val neighboursChildId = branchesCount * stars[i + 1].id.toInt() - 1
                        val neighboursChildIndex = neighboursChildId - 1
                        stars[neighboursChildIndex]
                    } else {
                        stars[i + 1]
                    }

                val edge = Edge(++edgesCreated, stars[i], starTo)
                edges.add(edge)
            }
        }
        return edges
    }

    // endregion

}