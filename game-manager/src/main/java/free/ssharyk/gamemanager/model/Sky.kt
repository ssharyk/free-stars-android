package free.ssharyk.gamemanager.model

import android.os.Parcel
import android.os.Parcelable

data class Sky(
    val stars: List<Star>,
    val edges: List<Edge>
) : Parcelable {

    init {
        stars.forEach { it.level = 0 }

        edges.forEach {
            stars[it.starFrom.id.toInt() - 1].level++
            stars[it.starTo.id.toInt() - 1].level++
        }
    }

    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(Star.CREATOR)!!,
        parcel.createTypedArrayList(Edge.CREATOR)!!
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.let {
            it.writeList(stars)
            it.writeList(edges)
        }
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Sky> {
        override fun createFromParcel(parcel: Parcel): Sky = Sky(parcel)
        override fun newArray(size: Int): Array<Sky?> = arrayOfNulls(size)
    }
}