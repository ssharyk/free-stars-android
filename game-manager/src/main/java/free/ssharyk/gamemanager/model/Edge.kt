package free.ssharyk.gamemanager.model

import android.os.Parcel
import android.os.Parcelable

data class Edge(
    val id: Long,
    val starFrom: Star,
    val starTo: Star,
    internal val intersections: MutableList<Long> = mutableListOf()
) : Parcelable {
    val hasIntersections: Boolean
        get() = intersections.isNotEmpty()

    internal fun reset() {
        intersections.clear()
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.let {
            it.writeLong(id)
            it.writeParcelable(starFrom, flags)
            it.writeParcelable(starTo, flags)
        }
    }

    override fun describeContents(): Int = 0

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readParcelable(Star::class.java.classLoader)!!,
        parcel.readParcelable(Star::class.java.classLoader)!!
    )

    companion object CREATOR : Parcelable.Creator<Edge> {
        override fun createFromParcel(parcel: Parcel): Edge = Edge(parcel)
        override fun newArray(size: Int): Array<Edge?> = arrayOfNulls(size)
    }
}