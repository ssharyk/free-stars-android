package free.ssharyk.gamemanager.model

import android.os.Parcel
import android.os.Parcelable

class Star(
    val id: Long,
    x: Int,
    y: Int
) : Parcelable {

    var x: Int = x
        private set
    var y: Int = y
        private set

    var level: Int = 1
        internal set

    fun moveTo(x: Int, y: Int) {
        this.x = x
        this.y = y
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Star) return false

        return this.x == other.x && this.y == other.y && this.level == other.level
    }

    override fun toString(): String {
        return "Star(id=$id, x=$x, y=$y, level=$level)"
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.let {
            it.writeLong(id)
            it.writeInt(x)
            it.writeInt(y)
            it.writeInt(level)
        }
    }

    override fun describeContents(): Int = 0

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readInt(),
        parcel.readInt()
    )

    companion object CREATOR : Parcelable.Creator<Star> {
        override fun createFromParcel(parcel: Parcel): Star = Star(parcel)
        override fun newArray(size: Int): Array<Star?> = arrayOfNulls(size)
    }
}