package free.ssharyk.gamemanager.intersection

import free.ssharyk.gamemanager.model.Edge
import free.ssharyk.gamemanager.model.Star

internal data class Line(val x1: Int, val y1: Int, val x2: Int, val y2: Int) {

    constructor(edge: Edge) : this(edge.starFrom.x, edge.starFrom.y, edge.starTo.x, edge.starTo.y)

    constructor(star1: Star, star2: Star) : this(star1.x, star1.y, star2.x, star2.y)
}