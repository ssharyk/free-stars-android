package free.ssharyk.gamemanager.intersection

import free.ssharyk.gamemanager.model.Edge
import kotlin.math.max
import kotlin.math.min

internal object IntersectionChecker {
    fun hasIntersection(line1: Line, line2: Line): Boolean {
        val (x1, y1, x2, y2) = line1
        val (x3, y3, x4, y4) = line2

        // intersection point
        val Zn = ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)).toFloat()
        val ChX = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)).toFloat()
        val ChY = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)).toFloat()
        val px = ChX / Zn
        val py = ChY / Zn

        // is inside section
        return px >= min(x1, x2) && px <= max(x1, x2) && py >= min(y1, y2) && py <= max(y1, y2) &&
                px >= min(x3, x4) && px <= max(x3, x4) && py >= min(y3, y4) && py <= max(y3, y4)
    }

    fun hasIntersection(edge1: Edge, edge2: Edge): Boolean {
        if (isParallel(edge1, edge2)) {
            return hasIntersectionOnParallel(edge1, edge2)
        }
        if (hasCommonStar(edge1, edge2)) return false
        return hasIntersection(Line(edge1), Line(edge2))
    }

    private fun hasIntersectionOnParallel(edge1: Edge, edge2: Edge): Boolean {
        if (isSame(edge1, edge2)) {

            val l1 = Line(edge1)
            val l2 = Line(edge2)

            val x1 = min(l1.x1, l1.x2)
            val x2 = max(l1.x1, l1.x2)
            val x3 = min(l2.x1, l2.x2)
            val x4 = max(l2.x1, l2.x2)

            if (x3 in (x1 + 1) until x2 || x4 in (x1 + 1) until x2) return true
            if (x1 in (x3 + 1) until x4 || x2 in (x3 + 1) until x4) return true
            return false
        } else {
            // just parallel, not same line
            return false
        }
    }

    fun hasCommonStar(edge1: Edge, edge2: Edge): Boolean {
        return edge1.starFrom == edge2.starFrom ||
                edge1.starFrom == edge2.starTo ||
                edge1.starTo == edge2.starFrom ||
                edge1.starTo == edge2.starTo
    }

    private fun isParallel(edge1: Edge, edge2: Edge): Boolean {
        val (x1, y1, x2, y2) = Line(edge1)
        val (x3, y3, x4, y4) = Line(edge2)

        if (x2 == x1) return x4 == x3
        if (x3 == x4) return x2 == x1

        val k1 = (y2.toFloat() - y1) / (x2 - x1)
        val k2 = (y4.toFloat() - y3) / (x4 - x3)

        return k1 == k2
    }

    private fun isSame(edge1: Edge, edge2: Edge): Boolean {
        val (x1, y1, x2, y2) = Line(edge1)
        val (x3, y3, x4, y4) = Line(edge2)

        if (x2 == x1) return x4 == x3
        if (x3 == x4) return x2 == x1

        val k1 = (y2.toFloat() - y1) / (x2 - x1)
        val k2 = (y4.toFloat() - y3) / (x4 - x3)

        val b1 = y1 - k1 * x1
        val b2 = y3 - k2 * x3

        return b1 == b2
    }
}